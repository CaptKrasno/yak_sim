#include "yak_plugin.h"


YakPlugin::YakPlugin()
{
  _thrust=0;
  _rudder=0;
}


void YakPlugin::Load(gazebo::physics::ModelPtr model, sdf::ElementPtr sdf){
  // Safety check
  if (model->GetJointCount() == 0)
  {
    std::cerr << "Invalid joint count, yak_plugin not loaded\n";
    return;
  }
  _model=model;

  _rudderJoint = _model->GetJoints()[0];
//  _rudderPid=gazebo::common::PID(100000, 0, 0);
//  _model->GetJointController()->SetPositionPID(this->_rudderJoint->GetScopedName(), this->_rudderPid);


  _updateConnection = gazebo::event::Events::ConnectWorldUpdateBegin(
            boost::bind(&YakPlugin::OnUpdate, this, _1));

  // Initialize ros, if it has not already bee initialized.
  if (!ros::isInitialized())
  {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, "gazebo_client",
        ros::init_options::NoSigintHandler);
  }

  this->_rosNode.reset(new ros::NodeHandle("gazebo_client"));

  ros::SubscribeOptions so =
    ros::SubscribeOptions::create<rom_yak::ThrusterCmdStamped>(
        "/cmd_thrust",
        1,
        boost::bind(&YakPlugin::thrustCallback, this, _1),
        ros::VoidPtr(), &this->rosQueue);
  this->_thrusterSub = this->_rosNode->subscribe(so);

  this->rosQueueThread =
    std::thread(std::bind(&YakPlugin::QueueThread, this));

  //_rosNode.subscribe("/cmd_thrust",10, &YakPlugin::thrustCallback, this);
}

void YakPlugin::QueueThread(){

  static const double timeout = 0.01;
  while (this->_rosNode->ok())
  {
    this->rosQueue.callAvailable(ros::WallDuration(timeout));
  }
}

void YakPlugin::OnUpdate(const gazebo::common::UpdateInfo &_info){
  gazebo::math::Vector3 force(_thrust,0,0);
  _model->GetLink("propeller")->AddRelativeForce(force);

  //this->_model->GetJointController()->SetPositionTarget(this->_rudderJoint->GetScopedName(), _rudder);
  this->_model->GetJointController()->SetJointPosition(_rudderJoint, _rudder);
}


void YakPlugin::thrustCallback(const rom_yak::ThrusterCmdStamped::ConstPtr &msg){
  _thrust=msg->thruster.thrust*50;
  _rudder=msg->thruster.angle;
  return;
}



