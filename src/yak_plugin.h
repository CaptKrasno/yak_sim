#ifndef YAK_PLUGIN_H
#define YAK_PLUGIN_H

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <thread>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "std_msgs/Float32.h"
#include <sensor_msgs/Joy.h>
#include "rom_yak/ThrusterCmdStamped.h"

class YakPlugin: public gazebo::ModelPlugin{
public:
  YakPlugin();
  virtual void Load(gazebo::physics::ModelPtr model, sdf::ElementPtr sdf);
  void OnUpdate(const gazebo::common::UpdateInfo & _info);
  void thrustCallback(const rom_yak::ThrusterCmdStamped::ConstPtr &msg);

private:
  float _thrust,_rudder;
  gazebo::physics::ModelPtr _model;
  gazebo::physics::JointPtr _rudderJoint;
  gazebo::common::PID _rudderPid;
  gazebo::event::ConnectionPtr _updateConnection;

  std::unique_ptr<ros::NodeHandle> _rosNode;
  ros::Subscriber _thrusterSub;

  ros::CallbackQueue rosQueue;
  std::thread rosQueueThread;


  void QueueThread();
};

GZ_REGISTER_MODEL_PLUGIN(YakPlugin);

#endif // YAK_PLUGIN_H
