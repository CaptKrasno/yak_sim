#ifndef _VELODYNE_PLUGIN_HH_
#define _VELODYNE_PLUGIN_HH_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <thread>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "std_msgs/Float32.h"
#include <sensor_msgs/Joy.h>


namespace gazebo
{
  /// \brief A plugin to control a Velodyne sensor.
  class KayakPlugin : public ModelPlugin
  {
    /// \brief Constructor
    public: KayakPlugin() {}
      public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
      {
        // Safety check
        if (_model->GetJointCount() == 0)
        {
          std::cerr << "Invalid joint count, Velodyne plugin not loaded\n";
          return;
        }
        thrust=0;
        rudder=0;
        this->model = _model;
        this->joint = _model->GetJoints()[0];
        this->pid = common::PID(100000, 0, 0);
        this->model->GetJointController()->SetPositionPID(
            this->joint->GetScopedName(), this->pid);
        this->model->GetJointController()->SetPositionTarget(
            this->joint->GetScopedName(), 0);
        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
                  boost::bind(&KayakPlugin::OnUpdate, this, _1));

        // Initialize ros, if it has not already bee initialized.
        if (!ros::isInitialized())
        {
          int argc = 0;
          char **argv = NULL;
          ros::init(argc, argv, "gazebo_client",
              ros::init_options::NoSigintHandler);
        }

        // Create our ROS node. This acts in a similar manner to
        // the Gazebo node
        this->rosNode.reset(new ros::NodeHandle("gazebo_client"));

        // Create a named topic, and subscribe to it.
        ros::SubscribeOptions so =
          ros::SubscribeOptions::create<std_msgs::Float32>(
              "/" + this->model->GetName() + "/thruster_cmd",
              1,
              boost::bind(&KayakPlugin::OnThrustMsg, this, _1),
              ros::VoidPtr(), &this->rosQueue);
        this->rosSub = this->rosNode->subscribe(so);

        ros::SubscribeOptions so1 = ros::SubscribeOptions::create<std_msgs::Float32>(
              "/" + this->model->GetName() + "/rudder_cmd",
              1,
              boost::bind(&KayakPlugin::OnRudderMsg, this, _1),
              ros::VoidPtr(), &this->rosQueue);
        this->rosSub1 = this->rosNode->subscribe(so1);

        ros::SubscribeOptions joySO = ros::SubscribeOptions::create<sensor_msgs::Joy>(
              "/joy",
              1,
              boost::bind(&KayakPlugin::OnJoyMsg, this, _1),
              ros::VoidPtr(), &this->rosQueue);
        this->joySub = this->rosNode->subscribe(joySO);

        // Spin up the queue helper thread.
        this->rosQueueThread =
          std::thread(std::bind(&KayakPlugin::QueueThread, this));
      }
      void OnUpdate(const common::UpdateInfo & /*_info*/)
      {
          math::Vector3 force(thrust,0,0);
          model->GetLink("propeller")->AddRelativeForce(force);
          this->model->GetJointController()->SetPositionTarget(
              this->joint->GetScopedName(), rudder);
      }

      /// \brief Handle an incoming message from ROS
      /// \param[in] _msg A float value that is used to set the velocity
      /// of the Velodyne.
      public: void OnThrustMsg(const std_msgs::Float32ConstPtr &_msg)
      {
        thrust=_msg->data;
        //this->SetVelocity(_msg->data);
      }

      public: void OnRudderMsg(const std_msgs::Float32ConstPtr &_msg)
      {
//        this->model->GetJointController()->SetPositionTarget(
//            this->joint->GetScopedName(), _msg->data);
      }

      public: void OnJoyMsg(const sensor_msgs::Joy::ConstPtyour &_msg)
      {
        thrust=_msg->axes[1]*50;
        rudder=_msg->axes[2]*3.14* -0.2;
      }

      /// \brief ROS helper function that processes messages
      private: void QueueThread()
      {
        static const double timeout = 0.01;
        while (this->rosNode->ok())
        {
          this->rosQueue.callAvailable(ros::WallDuration(timeout));
        }
      }


      /// \brief Pointer to the model.
      private: physics::ModelPtr model;

      /// \brief Pointer to the joint.
      private: physics::JointPtr joint;

      /// \brief A PID controller for the joint.
      private: common::PID pid;

      private: event::ConnectionPtr updateConnection;

      /// \brief A node use for ROS transport
      private: std::unique_ptr<ros::NodeHandle> rosNode;

      /// \brief A ROS subscriber
      private: ros::Subscriber rosSub;

      private: ros::Subscriber rosSub1;

      private: ros::Subscriber joySub;

      /// \brief A ROS callbackqueue that helps process messages
      private: ros::CallbackQueue rosQueue;

      /// \brief A thread the keeps running the rosQueue
      private: std::thread rosQueueThread;

      private: float thrust;
      private: float rudder;
  };

  // Tell Gazebo about this plugin, so that Gazebo can call Load on this plugin.
  GZ_REGISTER_MODEL_PLUGIN(KayakPlugin);
}
#endif
